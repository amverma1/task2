from SalesTaxCalculate.CalculateSalesTax import SalseTaxCalculator
from util.utility import unique_name_check, path_join

if __name__ == "__main__":

    obj = SalseTaxCalculator(
        path_join([".", "ProductCatalog.csv"]), path_join([".", "SalesTax.csv"]))
    obj.calculate_tax()
